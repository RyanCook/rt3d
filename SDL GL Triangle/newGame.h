#pragma once
#include "Player.h"
#include "SkyBox.h"
#include "Cube.h"


class newGame
{
public:
	newGame(void);
	~newGame(void);
	SDL_Window * setupRC(SDL_GLContext &context);
	void run();
	void update();
	void init();
	void checkCollision();
	GLuint loadBitmap(char *fname);
	void draw(SDL_Window * window);
private:
	Player *player;
	SkyBox *skybox;
	Cube *cubes;
	SDL_Window *window;
	GLfloat scale;
	bool colliding;
	bool otherColliding;
	GLuint skyBoxProgram;
	GLuint shaderProgram;
	GLuint translateRoad;
	std::stack<glm::mat4> mvStack;
	GLuint textures[15];
};

