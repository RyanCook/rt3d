#include "SkyBox.h"



GLfloat squareVertices[] = {
	-2.0f, -2.0f, -2.0f,
	-2.0f, 2.0f, -2.0f,
	2.0f, 2.0f, -2.0f,
	2.0f, -2.0f, -2.0f};

	GLuint squareIndices[] = { 
		0,1,2, 2,3,0 // square 
	}; 
	GLfloat squareTexCoords[] = {
		0.0f, 0.0f,
		0.0f, 1.0f,
		1.0f, 1.0f,
		1.0f, 0.0f};
		GLuint meshObj[1];

		SkyBox::SkyBox(void)
		{
			squareVertexCount = 4;
			squareIndexCount = 6;
		};



		SkyBox::~SkyBox(void)
		{
		};

		GLuint SkyBox::loadSkymap(char *fname)
		{
			GLuint texID;
			glGenTextures(1, &texID);// generate texture ID

			//load filr using core SDL library
			SDL_Surface *tmpSurface;
			tmpSurface = SDL_LoadBMP(fname);
			if(!tmpSurface)
			{
				std::cout << "Error loading bitmap" << std::endl;
			}

			//bind texture and set parameters
			glBindTexture(GL_TEXTURE_2D, texID);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

			SDL_PixelFormat *format = tmpSurface->format;
			GLuint externalFormat, internalFormat;
			if(format->Amask)
			{
				internalFormat = GL_RGBA;
				externalFormat =(format->Rmask < format->Bmask) ? GL_RGBA : GL_BGRA;
			}
			else{
				internalFormat = GL_RGB;
				externalFormat =(format->Rmask < format->Bmask) ? GL_RGB : GL_BGR;
			}

			glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, tmpSurface->w, tmpSurface->h, 0, externalFormat,
				GL_UNSIGNED_BYTE, tmpSurface->pixels);
			glGenerateMipmap(GL_TEXTURE_2D);
			SDL_FreeSurface(tmpSurface);
			return texID;
		};




		void SkyBox::initSkyTextures()
		{
			skyboxTexs[SKY_FRONT] = loadSkymap("front.bmp");
			skyboxTexs[SKY_BACK] = loadSkymap("back.bmp");
			skyboxTexs[SKY_LEFT] = loadSkymap("left.bmp");
			skyboxTexs[SKY_RIGHT] = loadSkymap("right.bmp");
			meshObj[0] = rt3d::createMesh(squareVertexCount, squareVertices,nullptr, squareVertices, squareTexCoords, squareIndexCount, squareIndices);
		};


		void SkyBox::drawSquares(GLuint chooseTex)
		{
			if(chooseTex==1)
			{
				glBindTexture(GL_TEXTURE_2D, skyboxTexs[SKY_BACK]);
			}
			if(chooseTex==2)
			{
				glBindTexture(GL_TEXTURE_2D, skyboxTexs[SKY_RIGHT]);
			}
			if(chooseTex==3)
			{
				glBindTexture(GL_TEXTURE_2D, skyboxTexs[SKY_FRONT]);
			}
			if(chooseTex==4)
			{
				glBindTexture(GL_TEXTURE_2D, skyboxTexs[SKY_LEFT]);
			}
			rt3d::drawIndexedMesh(meshObj[0],squareIndexCount,GL_TRIANGLES);
		};



