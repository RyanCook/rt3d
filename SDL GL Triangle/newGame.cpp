#include "newGame.h"
using namespace std;


GLfloat zPos, xPos;
glm::vec4 lightPos(0.0f, 10.0f, -5.0f, 1.0f);


newGame::newGame(void)
{

	SDL_GLContext glContext; // OpenGL context handle
	window = setupRC(glContext); // Create window and render context 

	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << endl;
		exit (1);
	}
	cout << glGetString(GL_VERSION) << endl;
	scale = 1.0f;
	translateRoad = 1.0f;
	colliding = false;
	otherColliding = false;

}


newGame::~newGame(void)
{
}

GLuint newGame::loadBitmap(char *fname)
{
	GLuint texID;
	glGenTextures(1, &texID);// generate texture ID

	//load filr using core SDL library
	SDL_Surface *tmpSurface;
	tmpSurface = SDL_LoadBMP(fname);
	if(!tmpSurface)
	{
		std::cout << "Error loading bitmap" << std::endl;
	}

	//bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	SDL_PixelFormat *format = tmpSurface->format;
	GLuint externalFormat, internalFormat;
	if(format->Amask)
	{
		internalFormat = GL_RGBA;
		externalFormat =(format->Rmask < format->Bmask) ? GL_RGBA : GL_BGRA;
	}
	else{
		internalFormat = GL_RGB;
		externalFormat =(format->Rmask < format->Bmask) ? GL_RGB : GL_BGR;
	}

	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, tmpSurface->w, tmpSurface->h, 0, externalFormat,
		GL_UNSIGNED_BYTE, tmpSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);
	SDL_FreeSurface(tmpSurface);
	return texID;
};


SDL_Window* newGame::setupRC(SDL_GLContext &context) {
	if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
		rt3d::exitFatalError("Unable to initialize SDL"); 

	// Request an OpenGL 3.0 context.
	// Not able to use SDL to choose profile (yet), should default to core profile on 3.2 or later
	// If you request a context not supported by your drivers, no OpenGL context will be created

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)

	// Create 800x600 window
	window = SDL_CreateWindow("SDL/GLM/OpenGL Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
		rt3d::exitFatalError("Unable to create window");

	context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
	SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
	return window;
}
//where objects and textures are created.
void newGame::init()
{
	player = new Player();
	skybox = new SkyBox();
	cubes = new Cube();
	shaderProgram = rt3d::initShaders("phong-tex.vert","phong-tex.frag");
	skyBoxProgram = rt3d::initShaders("texture.vert","texture.frag");
	skybox->initSkyTextures();
	player->init();
	cubes->cubeInit();
	glEnable(GL_DEPTH_TEST); // enable depth testing
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	textures[0] = loadBitmap("fabric.bmp");
	textures[1] = loadBitmap("studdedmetal.bmp");
	textures[2] = loadBitmap("hobgoblin2.bmp");
	textures[3] = loadBitmap("Road.bmp");
	textures[4] = loadBitmap("FrontBuilding.bmp");
	textures[5] = loadBitmap("pavement.bmp");
	textures[6] = loadBitmap("grass.bmp");
	textures[7] = loadBitmap("wall.bmp");
	textures[8] = loadBitmap("tallBuilding.bmp");
	textures[9] = loadBitmap("House.bmp");
	textures[10] = loadBitmap("outerhedge.bmp");
	textures[11] = loadBitmap("Path.bmp");
}

void newGame::checkCollision()
{
	if(player->getPlayerPos().x <-16.0f)
		player->setPlayerx(-16.0f);
	if(player->getPlayerPos().x >77.5f)
		player->setPlayerx(77.5f);
	if(player->getPlayerPos().z >84.5f)
		player->setPlayerz(84.5f);
	if(player->getPlayerPos().z <-14.0f)
		player->setPlayerz(-14.0f);
	if(player->getPlayerPos().x > 5.0f && player->getPlayerPos().x <8.0f && player->getPlayerPos().z > 15.0f && player->getPlayerPos().z <31.0f)
	{
		colliding = true;
		xPos = player->getPlayerPos().x;
		zPos = player->getPlayerPos().z;
	};

	if(player->getPlayerPos().x < 8.5f && player->getPlayerPos().x >3.5f && player->getPlayerPos().z > 40.6f && player->getPlayerPos().z <94.2f)
	{
		colliding = true;
		xPos = player->getPlayerPos().x;
		zPos = player->getPlayerPos().z;
	}

	if(player->getPlayerPos().x > 8.5f && player->getPlayerPos().x <80.0f && player->getPlayerPos().z < 17.9f && player->getPlayerPos().z >15.0)
	{
		otherColliding = true;
		xPos = player->getPlayerPos().x;
		zPos = player->getPlayerPos().z;
	}

	if(player->getPlayerPos().x > 4.0f && player->getPlayerPos().x <55.0f && player->getPlayerPos().z >9.5f && player->getPlayerPos().z <15.0)
	{
		colliding = true;
		xPos = player->getPlayerPos().x;
		zPos = player->getPlayerPos().z;
	}
	if(player->getPlayerPos().x > 52.0f && player->getPlayerPos().x <56.0f && player->getPlayerPos().z >-17.0f && player->getPlayerPos().z <10.0)
	{
		colliding = true;
		xPos = player->getPlayerPos().x;
		zPos = player->getPlayerPos().z;
	}
	if(colliding==true)
	{
		player->setPlayerx(xPos-=0.1f);
		player->setPlayerz(zPos-=0.1f);
	}colliding = false;

	if(otherColliding==true)
	{
		//player->setPlayerx(xPos+=0.1f);
		player->setPlayerz(zPos+=0.1f);
	}otherColliding = false;
}

void newGame::update(void)
{
	player->playerControls();
	checkCollision();
}



void newGame::draw(SDL_Window * window) {
	// clear the screen
	glClearColor(0.5f,0.5f,0.5f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	rt3d::lightStruct light0 = 
	{ {1.0f, 1.0f, 1.0f, 1.0f}, // ambient 
	{0.0f, 0.0f, 0.0f, 1.0f}, // diffuse 
	{1.0f, 1.0f, 1.0f, 1.0f}, // specular
	{0.0f, 0.0f, -4.0f, 0.0f} // position 
	};
	rt3d::materialStruct material0 = 
	{ {1.0f, 1.0f, 1.0f, 1.0f}, // ambient
	{1.0f, 1.0f, 1.0f, 1.0f}, // diffuse
	{1.0f, 1.0f, 1.0f, 1.0f}, // specular 
	2.0f // shininess 
	}; 
	//making sure light stays the same
	mvStack.push(glm::mat4(1.0));
	rt3d::setLight(shaderProgram, light0); 
	rt3d::setMaterial(shaderProgram, material0);
	glm::mat4 projection(1.0);
	projection = glm::perspective(60.0f,800.0f/600.0f,1.0f,200.0f);
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(projection));


	//setting up the camera.
	glm::mat4 modelview(1.0);
	mvStack.push(modelview);
	mvStack.top() = player->returnLookAt();
	glm::vec4 tmp = mvStack.top()*lightPos;
	//SKYBOX CODE
	glUseProgram(skyBoxProgram);
	rt3d::setUniformMatrix4fv(skyBoxProgram, "projection", glm::value_ptr(projection));
	glDepthMask(GL_FALSE);
	glm::mat3 mvRotOnlyMat3 = glm::mat3(mvStack.top());
	mvStack.push( glm::mat4(mvRotOnlyMat3));

	//back
	mvStack.push(mvStack.top());
	mvStack.top() = glm::rotate(mvStack.top(), 180.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(skyBoxProgram, "modelview", glm::value_ptr(mvStack.top()));
	skybox->drawSquares(1);
	//left
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	rt3d::setUniformMatrix4fv(skyBoxProgram, "modelview", glm::value_ptr(mvStack.top()));
	skybox->drawSquares(2);
	//front
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	rt3d::setUniformMatrix4fv(skyBoxProgram, "modelview", glm::value_ptr(mvStack.top()));
	skybox->drawSquares(3);

	//right
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	rt3d::setUniformMatrix4fv(skyBoxProgram, "modelview", glm::value_ptr(mvStack.top()));
	skybox->drawSquares(4);
	mvStack.pop();
	mvStack.pop();


	//assuring that the lighting stays the same, done after camera but before 
	//actually rendering any objects, passing light info to the shader
	light0.position[0] = tmp.x;
	light0.position[1] = tmp.y;
	light0.position[2] = tmp.z;
	rt3d::setLightPos(shaderProgram, glm::value_ptr(tmp));
	rt3d::setLight(shaderProgram, light0);

	// draw Terrain
	//Road
	glDepthMask(GL_TRUE);

	glUseProgram(shaderProgram);
	glBindTexture(GL_TEXTURE_2D, textures[3]);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(translateRoad, -0.2f, -10.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(5.0f, -0.1f, 15.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::setLight(shaderProgram, light0);
	rt3d::setMaterial(shaderProgram, material0);
	cubes->cubeDraw();


	for(int i=0; i<10;i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(translateRoad, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	}
	mvStack.pop();

	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(translateRoad, -0.2f, -10.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(15.0f, -0.1f, 15.0f));
	mvStack.top() = glm::rotate(mvStack.top(), 270.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::setLight(shaderProgram, light0);
	rt3d::setMaterial(shaderProgram, material0);
	cubes->cubeDraw();

	for(int i=0; i<6;i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(translateRoad, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	}

	mvStack.pop();

	//buildings
	glBindTexture(GL_TEXTURE_2D, textures[4]);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-19.0f, 0.0f, -20.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(12.0f, 10.0f, 5.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::setLight(shaderProgram, light0);
	rt3d::setMaterial(shaderProgram, material0);
	cubes->cubeDraw();

	for(int i=0; i<5;i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	}
	mvStack.pop();
	glBindTexture(GL_TEXTURE_2D, textures[8]);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(40.0f, 0.0f, 10.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(12.0f, 17.0f, 5.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::setLight(shaderProgram, light0);
	rt3d::setMaterial(shaderProgram, material0);
	cubes->cubeDraw();

	glBindTexture(GL_TEXTURE_2D, textures[9]);
	for(int i=0; i<3;i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	}

	mvStack.pop();


	//pavement
	glBindTexture(GL_TEXTURE_2D, textures[5]);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(1.0f, -0.4f, -15.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(5.0f, 0.4f, 5.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::setLight(shaderProgram, light0);
	rt3d::setMaterial(shaderProgram, material0);
	cubes->cubeDraw();

	for(int i=0; i<10;i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();

	}

	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f, 4.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	cubes->cubeDraw();

	for(int i=0; i<10;i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	}
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-1.0f, 0.0f, 0.0f));

	for(int i=0; i<17;i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	}
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f, -4.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	cubes->cubeDraw();

	for(int i=0; i<22; i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	}

	mvStack.top() = glm::rotate(mvStack.top(), 270.0f, glm::vec3(0.0f, 1.0f, 0.0f));

		for(int i=0; i<3; i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	}

	mvStack.pop();

	//Wall

	glBindTexture(GL_TEXTURE_2D, textures[7]);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(53.0f, -0.2f, -16.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(2.0f, 5.0f, 15.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::setLight(shaderProgram, light0);
	rt3d::setMaterial(shaderProgram, material0);
	cubes->cubeDraw();

	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f, 1.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	cubes->cubeDraw();
	mvStack.pop();

	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-19.0f, 0.0f, -16.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(2.0f, 5.0f, 15.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::setLight(shaderProgram, light0);
	rt3d::setMaterial(shaderProgram, material0);
	cubes->cubeDraw();

	
	for(int i=0; i<6; i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f, 1.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	}

	mvStack.pop();

	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-18.0f, 0.0f, 85.5f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(25.0f, 5.0f, 2.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::setLight(shaderProgram, light0);
	rt3d::setMaterial(shaderProgram, material0);
	cubes->cubeDraw();
	mvStack.pop();

	// Grass Park
	//actual Grass
	glBindTexture(GL_TEXTURE_2D, textures[6]);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(8.0f, -0.2f, 15.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(10.0f, -0.1f, 10.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::setLight(shaderProgram, light0);
	rt3d::setMaterial(shaderProgram, material0);
	cubes->cubeDraw();

	for(int i=0; i<6; i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(1.0f, 0.0f, 1.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	};

	for(int i=0; i<6; i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	};

	for(int i=0; i<5; i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f, -1.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	};

	for(int i=0; i<4; i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(1.0f, 0.0f, 1.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	};

	for(int i=0; i<3; i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	};

	for(int i=0; i<2; i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f,-1.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	};

	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(1.0f, 0.0f,1.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	cubes->cubeDraw();
	mvStack.pop();

	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(8.0f, -0.2f, 15.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(10.0f, -0.1f, 10.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::setLight(shaderProgram, light0);
	rt3d::setMaterial(shaderProgram, material0);
	cubes->cubeDraw();

	for(int i=0; i<6; i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	};

	for(int i=0; i<5; i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f, 1.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	};

	for(int i=0; i<4; i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-1.0f, 0.0f, -1.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	};
	for(int i=0; i<3; i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	};

	for(int i=0; i<2; i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f, 1.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	};

	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-1.0f, 0.0f, -1.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	cubes->cubeDraw();
	mvStack.pop();

	//Hedges.
	glBindTexture(GL_TEXTURE_2D, textures[10]);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(6.0f, -0.2f, 15.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(2.0f, 5.0f, 15.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::setLight(shaderProgram, light0);
	rt3d::setMaterial(shaderProgram, material0);
	cubes->cubeDraw();

	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f, 1.8f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	cubes->cubeDraw();

	for(int i=0; i<3; i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f, 1.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	};

	mvStack.pop();

	//inner Hedges
	glBindTexture(GL_TEXTURE_2D, textures[10]);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(8.0f, -0.2f, 15.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(10.0f, 5.0f, 2.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::setLight(shaderProgram, light0);
	rt3d::setMaterial(shaderProgram, material0);
	cubes->cubeDraw();

	for(int i=0; i<7; i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	};


	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.2f, 1.0f, 5.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	cubes->cubeDraw();

	for(int i=0; i<7; i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f, 1.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	};

	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(-5.0f, 1.0f, 0.2f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	cubes->cubeDraw();

	for(int i=0; i<6; i++)
	{
		mvStack.top() = glm::translate(mvStack.top(), glm::vec3(1.0f, 0.0f, 0.0f));
		rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
		cubes->cubeDraw();
	};

	mvStack.pop();

	//path to Grass

	glBindTexture(GL_TEXTURE_2D, textures[11]);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(6.0f, -0.2f, 30.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(2.0f, 0.1f, 12.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::setLight(shaderProgram, light0);
	rt3d::setMaterial(shaderProgram, material0);
	cubes->cubeDraw();
	mvStack.pop();


	// player
	glBindTexture(GL_TEXTURE_2D, textures[2]);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(),  player->getPlayerPos());
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(-1.0f, 0.0f, 0.0f));
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(0.0f, 0.0f, -1.0f));
	mvStack.top() = glm::rotate(mvStack.top(), player->getPlayerRotation(), glm::vec3(0.0f, 0.0f, -1.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(scale*0.05, scale*0.05, scale*0.05));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::setLight(shaderProgram, light0);
	rt3d::setMaterial(shaderProgram, material0);
	player->drawPlayer();
	mvStack.pop();
	mvStack.pop();
	SDL_GL_SwapWindow(window); // swap buffers

}







void newGame::run()
{
	bool running = true; // set running to true
	SDL_Event sdlEvent; // variable to detect SDL events
	std::cout << "Progress: About to enter main loop" << std::endl;

	// unlike GLUT, SDL requires you to write your own event loop
	// This puts much more power in the hands of the programmer
	// This simple loop only responds to the window being closed.
	while (running)	// the event loop
	{
		while (SDL_PollEvent(&sdlEvent))
		{
			if (sdlEvent.type == SDL_QUIT)
				running = false;
		}
		draw(window);
		update();
	}

	SDL_DestroyWindow(window);
	SDL_Quit();
}