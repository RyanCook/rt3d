#pragma once
#include "rt3d.h"
#include "rt3dObjLoader.h"
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Cube
{
public:
	Cube(void);
	~Cube(void);
	void cubeDraw();
	void cubeInit();
private:
	GLuint meshIndexCount;
	GLuint size;
	std::vector<GLfloat> verts;
	std::vector<GLfloat> norms;
	std::vector<GLfloat> tex_coords;
	std::vector<GLuint> indices;
	GLuint meshObjects[1];
};

