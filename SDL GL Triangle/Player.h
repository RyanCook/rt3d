#pragma once
#include "rt3d.h"
#include "rt3dObjLoader.h"
#include "md2model.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "audio.h"

class Player
{
public:
	Player(void);
	~Player(void);
	void playerControls();
	void drawPlayer();
	void init();
	float getPlayerRotation();
	glm::vec3 getPlayerPos();
	glm::vec3 moveForwar(glm::vec3 x, GLfloat y, GLfloat z);
	glm::vec3 moveRigh(glm::vec3 pos, GLfloat angle, GLfloat d);
	GLfloat setPlayerx(GLfloat x);
	GLfloat setPlayerz(GLfloat z);
	glm::vec3 getEye();
	glm::mat4 returnLookAt();
private:
	GLuint md2VertCount;
	md2model tmpModel;
	GLuint currentAnim;
	GLfloat rotateMD2;
	GLfloat rotatePlayer;
	GLuint meshOb[2];
	bool running;
	audio *music;
};

