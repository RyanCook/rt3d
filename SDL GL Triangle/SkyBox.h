
#include "rt3d.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <stack>
#include <glm/gtc/type_ptr.hpp>

class SkyBox
{
public:
	SkyBox(void);
	~SkyBox(void);
	GLuint loadSkymap(char *fname);
	void drawSquares(GLuint x);
	void initSkyTextures();
private:
	GLuint squareVertexCount;
	GLuint squareIndexCount;
	enum {SKY_LEFT = 0, SKY_BACK, SKY_RIGHT, SKY_FRONT, SKY_TOP, SKY_BOTTOM};// automatically generates values
	unsigned int skyboxTexs[6];
};

