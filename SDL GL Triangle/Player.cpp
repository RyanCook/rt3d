#include "Player.h"
#define DEG_TO_RAD 0.017453293


glm::vec3 playerPosition(1.0, 1.0, 1.0);
glm::vec3 eye(0.0f, 0.0f, 0.0f);
glm::vec3 at(0.0f, 1.0f, 3.0f);
glm::vec3 up(0.0f, 1.0f, 0.0f);
glm::vec3 newEye(0.0f, 0.0f, 0.0f);




Player::Player(void)
{
	rotatePlayer = 0.0f;
	currentAnim = 0.0f;
	rotateMD2 = 0.0f;
	running = false;
	md2VertCount = 0;
	music = new audio();
}


Player::~Player(void)
{
}



void Player::init()
{
	meshOb[0] = tmpModel.ReadMD2Model("tris.MD2");
	md2VertCount = tmpModel.getVertDataSize();
	music->init();
	music->Play();
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
}


glm::vec3 Player::moveForwar(glm::vec3 cam, GLfloat angle, GLfloat d) //moveForward(glm::vec3 cam, GLfloat angle, GLfloat d)
{
	return glm::vec3(cam.x + d*std::sin(angle*DEG_TO_RAD), cam.y, cam.z - d*std::cos(angle*DEG_TO_RAD));
}

glm::vec3 Player::moveRigh(glm::vec3 pos, GLfloat angle, GLfloat d)
{
	return glm::vec3(pos.x + d*std::cos(angle*DEG_TO_RAD), pos.y, pos.z +  d*std::sin(angle*DEG_TO_RAD));
}


void Player::drawPlayer()
{
	//set the camera to follow the player, but be 6 back in the Z
	newEye = moveForwar(playerPosition, rotatePlayer, 6.0f);
	eye = newEye;
	// set the cameran 2 up in the y
	eye.y = newEye.y + 2.0f;
	at = playerPosition;
	rt3d::drawMesh(meshOb[0], md2VertCount, GL_TRIANGLES);
		// Animate the md2 model, and update the mesh with new vertex data
	tmpModel.Animate(currentAnim,0.1);
	rt3d::updateMesh(meshOb[0],RT3D_VERTEX,tmpModel.getAnimVerts(),tmpModel.getVertDataSize());
};

void Player::playerControls()
{
	Uint8 *keys = SDL_GetKeyboardState(NULL);
	if ( keys[SDL_SCANCODE_W] ){ playerPosition = moveForwar(playerPosition, rotatePlayer, -0.1);
	running = true; ;}
	if ( keys[SDL_SCANCODE_S] ) {playerPosition = moveForwar(playerPosition, rotatePlayer, 0.1); 
	running = true; ;}
	if ( keys[SDL_SCANCODE_A] ) {playerPosition = moveRigh(playerPosition, rotatePlayer, 0.1); 
	running = true;;}
	if ( keys[SDL_SCANCODE_D] ){ playerPosition = moveRigh(playerPosition, rotatePlayer, -0.1); 
	running = true; ;}
	if ( keys[SDL_SCANCODE_LEFT] ) rotatePlayer -= 0.5;
	if ( keys[SDL_SCANCODE_RIGHT] ) rotatePlayer += 0.5;
	if(running)
	{
		currentAnim = 1;
	}
	else currentAnim =0;
	running = false;

}
glm::mat4 Player::returnLookAt()
{
	return glm::lookAt(eye, at, up);
}

glm::vec3 Player::getPlayerPos()
{
	return playerPosition;
}

glm::vec3 Player::getEye()
{
	return eye;
}

float Player::getPlayerRotation()
{
	return rotatePlayer;
}

GLfloat Player::setPlayerx(GLfloat playerXPos)
{
	playerPosition.x = playerXPos;
	return playerPosition.x;
}
GLfloat Player::setPlayerz(GLfloat playerZPos)
{
	playerPosition.z = playerZPos;
	return playerPosition.z;
}
