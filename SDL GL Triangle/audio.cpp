#include "audio.h"


audio::audio(void)
{

	HSAMPLE *samples = nullptr;
}


audio::~audio(void)
{
}



HSAMPLE audio::loadSample(char * filename)
{
	HSAMPLE sam;
	if (sam=BASS_SampleLoad(FALSE,filename,0,0,3,BASS_SAMPLE_OVER_POS))
		std::cout << "sample " << filename << " loaded!" << std::endl;
	else
	{
		std::cout << "Can't load sample";
		exit (0);
	}
	return sam;
}

void audio::init(void) 
{
	/* Initialize default output device */
	if (!BASS_Init(-1,44100,0,0,NULL))
		std::cout << "Can't initialize device";

	samples = new HSAMPLE[2];
	// Following comment is from source basstest file!
	/* Load a sample from "file" and give it a max of 3 simultaneous
	playings using playback position as override decider */
	samples[0] = loadSample( "BackingTrack.mp3");
	std::cout << "Press 1 and 2 to play sounds." << std::endl;
	std::cout << "Press P and R to pause and resume." << std::endl;
}


void audio::Play()
{
	HCHANNEL ch=BASS_SampleGetChannel(samples[0],FALSE);
	BASS_ChannelSetAttributes(ch,-1,50,(rand()%201)-100);
	if (!BASS_ChannelPlay(ch,FALSE))
		std::cout << "Can't play sample" << std::endl;
}

	//Uint8 *keys = SDL_GetKeyboardState(NULL);
	//if ( keys[SDL_SCANCODE_1] ) {

	//	HCHANNEL ch=BASS_SampleGetChannel(samples[0],FALSE);
	//	BASS_ChannelSetAttributes(ch,-1,50,(rand()%201)-100);
	//	if (!BASS_ChannelPlay(ch,FALSE))
	//		std::cout << "Can't play sample" << std::endl;

	//}
	//
	//if ( keys[SDL_SCANCODE_2] ) {
	//	HCHANNEL ch=BASS_SampleGetChannel(samples[1],FALSE);
	//	BASS_ChannelSetAttributes(ch,-1,50,(rand()%201)-100);
	//	if (!BASS_ChannelPlay(ch,FALSE))
	//		std::cout << "Can't play sample" << std::endl;
	//}

	//if ( keys[SDL_SCANCODE_P] ) {
	//	BASS_Pause();
	//}

	//if ( keys[SDL_SCANCODE_R] ) {
	//	BASS_Start();
	//}
