#pragma once
#include<iostream>
#include<SDL.h>
#include<ctime>
#include"bass.h"

class audio
{
public:
	audio(void);
	~audio(void);
	HSAMPLE loadSample(char * filename);
	void init(void);
	void Play();
	void setClock();

private:
	HSAMPLE *samples;
};

