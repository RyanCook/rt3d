#include "Cube.h"


Cube::Cube(void)
{
	meshIndexCount = 0;
}


Cube::~Cube(void)
{
}

void Cube::cubeInit()
{
	rt3d::loadObj("cube.obj", verts, norms, tex_coords, indices);
	size = indices.size();
	meshIndexCount = size;
	meshObjects[0] = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());
}

void Cube::cubeDraw()
{
	rt3d::drawMesh(meshObjects[0], meshIndexCount, GL_TRIANGLES);
}